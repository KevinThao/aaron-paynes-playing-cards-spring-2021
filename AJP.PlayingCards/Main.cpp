//Aaron Payne
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit

enum Rank {
	Two = 2, Three,	Four, Five,	Six,
	Seven, Eight, Nine,	Ten,
	Jack, Queen, King, Ace
};

enum Suit {
	Hearts, Clubs, Spades, Diamonds
};

// struct for card
struct Card {
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	switch (card.rank)
	{
		case 2:
			cout << "The Two of ";
			break;
		case 3:
			cout << "The Three of ";
			break;
		case 4:
			cout << "The Four of ";
			break;
		case 5:
			cout << "The Five of ";
			break;
		case 6:
			cout << "The Six of ";
			break;
		case 7:
			cout << "The Seven of ";
			break;
		case 8:
			cout << "The Eight of ";
			break;
		case 9:
			cout << "The Nine of ";
			break;
		case 10:
			cout << "The Ten of ";
			break;
		case 11:
			cout << "The Jack of ";
			break;
		case 12:
			cout << "The Queen of ";
			break;
		case 13:
			cout << "The King of ";
			break;
		case 14:
			cout << "The Ace of ";;
			break;
			
	}

	switch (card.suit)
	{
		case 0:
			cout << "Hearts." << "\n\n";
			break;
		case 1:
			cout << "Clubs." << "\n\n";
			break;
		case 2:
			cout << "Spades." << "\n\n";
			break;
		case 3:
			cout << "Diamonds." << "\n\n";
			break;
	}

	//cout << "The " << card.rank << " of " << card.suit << "\n";
}

Card HighCard(Card firstCard, Card secondCard)
{
	if (firstCard.rank > secondCard.rank)
	{
		return firstCard;
	}
	else if (firstCard.rank < secondCard.rank)
	{
		return secondCard;
	}
	else
	{
		// if the rank of both cards are tied then return the first card
		return firstCard;
	}
}

int main()
{
	cout << "Print out the cards written in the code, should be Ten of Hearts, Ace of Spades, " << "\n" <<
		"Jack of Clubs, and Jack of Diamonds." << "\n\n";

	Card a;
	a.rank = Ten;
	a.suit = Hearts;
	// this should print out "The Ten of Hearts."
	PrintCard(a);

	Card b;
	b.rank = Ace;
	b.suit = Spades;
	// this should print out "The Ace of Spades."
	PrintCard(b);

	// adding 2 more different cards to make sure they print correctly
	Card c;
	c.rank = Jack;
	c.suit = Clubs;
	PrintCard(c);

	Card d;
	d.rank = Jack;
	d.suit = Diamonds;
	PrintCard(d);

	// this should print out "The Ace of Spades" since Ace ranking is higher than Ten
	cout << "Which of the cards rank higher? Ten of Hearts or Ace of Spades?" << "\n\n";
	PrintCard(HighCard(a, b));

	// since both rank the same, it should print the first card, which is Jack of Clubs, even though the suit is smaller, the rank is the same, so print first card given.
	cout << "Which of the cards rank higher? Jack of Clubs or Jack of Diamonds?" << "\n\n";
	PrintCard(HighCard(c, d));

	/*
	if (a.rank < b.rank)
	{
		cout << "Ace of Spades it greater than the Jack of Hearts\n";
	}
	*/



	(void)_getch();
	return 0;
}